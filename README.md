# Portfolio Client
For my personal portfolio site, built using [Vue.js](https://vuejs.org/).  
[API Source](https://gitlab.com/Mantik/portfolio-2019-api/)  
[Client source](https://gitlab.com/Mantik/portfolio-2019-site/)  

## Project setup
Requires [Yarn](https://yarnpkg.com/).
```
yarn install
```

### Compile and hot-reload for development
```
yarn serve
```

### Compile and minify for production
```
yarn build
```

### Run tests
```
yarn test
```

### Lint and fix files
```
yarn lint
```
