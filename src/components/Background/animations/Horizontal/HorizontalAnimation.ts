import Particle from './Particle'
import Vector2 from '../../core/Vector2'
import Animation from '../../core/Animation'


/**
 *  Particle Animation
 */
export default class HorizontalAnimation extends Animation
{
    /**
     * radius of the particles
     */
    protected particle_radius:number = 2


    /**
     * counter used for particle_wait
     */
    protected particle_counter:number = 0


    /**
     * How many frames to wait to spawn a particle
     */
    protected readonly particle_wait:number = 7


    
    /**
     * @param canvas canvas to draw on
     * @param particle_amount amount of particles to spawn
     */
    public constructor(canvas: HTMLCanvasElement, particle_amount: number)
    {
        super(canvas)

        // create particles for requested amount
        for(let i=0; i<particle_amount; ++i)
            this.addRandomParticle()
    }



    public update()
    {        
        // spawn a particle every x frames
        this.particle_counter++
        if(this.particle_counter >= this.particle_wait)
        {
            this.particle_counter = 0
            this.addRandomParticle(-1)
        }
        
        super.update()
    }



    /**
     * Add an particle
     * @param amount    how many particle to add
     */
    protected addParticle(position:Vector2)
    {
        const velocity = new Vector2()
        const particle = new Particle(this.context, position, 0.5, this.particle_radius, 1, velocity)
        this.objects.push(particle)
        
        return particle
    }


    protected addRandomParticle(max_x:any=null)
    {
        if(max_x===null)
            max_x = this.canvas.width

        // random position
        const position = new Vector2()
            .randomise(0, max_x, 'x')
            .randomise(0, this.canvas.height, 'y')

        // add particle
        this.addParticle(position)
    }
}
