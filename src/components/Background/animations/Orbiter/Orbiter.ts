import PhysicalObject from '../../core/PhysicalObject'
// import Trail from './Trail'
import Vector2 from '../../core/Vector2'


/**
 * A particle on screen
 */
export default class Orbiter extends PhysicalObject
{
    /**
     * Trail
     */
    // private trail: Trail


    constructor(context:CanvasRenderingContext2D, position:Vector2, mass:number, radius:number, restitution:number, velocity:Vector2)
    {
        super(context, position, mass, radius, restitution, velocity)
        // this.trail = new Trail(this)
    }


    public draw()
    {
        this.context.beginPath()
        this.context.fillStyle = "#FFF"
        this.context.arc(this.position.x, this.position.y, this.radius, 0, 2 * Math.PI)
        this.context.fill()
    }


    public update()
    {
        this.position.x += 1
    }
}
