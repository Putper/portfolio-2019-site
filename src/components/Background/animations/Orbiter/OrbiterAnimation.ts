import Animation from '../../core/Animation'
import Orbiter from './Orbiter'
import Vector2 from '../../core/Vector2'


/**
 *  Particle Animation
 */
export default class OrbiterAnimation extends Animation
{
    /**
     * gravity point
     */
    protected gravity_pos: Vector2 = new Vector2(0,0)

    
    /**
     * radius of the orbiters
     */
    protected orbiter_radius: number = 2


    
    /**
     * @param canvas canvas to draw on
     * @param orbiter_amount amount of orbits to spawn
     */
    public constructor(canvas: HTMLCanvasElement, orbiter_amount: number)
    {
        super(canvas)

        // create orbiters for requested amount
        for(let i=0; i<orbiter_amount; ++i)
            this.addRandomOrbiter()
    }



    /**
     * set mouse to gravity pos
     */
    public onMouseMove = (event:MouseEvent) =>
    {
        this.gravity_pos.x = event.clientX
        this.gravity_pos.y = event.clientY
    }


    /**
     * reset gravity pos
     */
    public onMouseOut = (event:MouseEvent) =>
    {
        this.gravity_pos.x = this.canvas.width/2
        this.gravity_pos.y = this.canvas.height/2
    }



    /**
     * Add an orbiter
     * @param amount    how many orbiters to add
     */
    protected addOrbiter(position:Vector2)
    {
        const velocity = new Vector2()
        const orbiter = new Orbiter(this.context, position, 0.5, this.orbiter_radius, 1, velocity)
        this.objects.push(orbiter)
        
        return orbiter
    }


    protected addRandomOrbiter()
    {
        // random position
        const position = new Vector2()
            .randomise(0, this.canvas.width, 'x')
            .randomise(0, this.canvas.height, 'y')

        // add orbiter
        this.addOrbiter(position)
    }
}
