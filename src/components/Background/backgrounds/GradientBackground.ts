/**
 * A coloured background
 */
export default class GradientBackground
{
    /**
     * @var CanvasRenderingContext2D
     */
    protected readonly context: CanvasRenderingContext2D


    /**
     * @var HTMLCanvasElement
     */
    protected readonly canvas: HTMLCanvasElement


    /**
     * Background colour of the canvas
     * @var string  a css colour value in string (hex value, rgb(), rgba())
     */
    protected background_colour: string = 'rgb(11, 76, 61)'


    
    /**
     * @param canvas canvas to draw on
     * @param orbiter_amount amount of orbits to spawn
     */
    public constructor(canvas: HTMLCanvasElement)
    {
        this.canvas = canvas
        this.context = <CanvasRenderingContext2D>this.canvas.getContext('2d')
        this.drawBackground()
    }



    /**
     * redraw on resize
     */
    public onResize = () =>
    {
        this.drawBackground()
    }


    /**
     * Draws the background
     */
    protected drawBackground()
    {
        // canvas radius
        let cx = this.canvas.width * 0.5, // radius-x
            cy = this.canvas.height * 0.5 // radius-y
        // background gradient
        let gradient = this.context.createRadialGradient(cx, cy, 0, cx, cy, Math.sqrt(cx * cx + cy * cy))
        gradient.addColorStop(0, 'rgba(0, 0, 0, 0)')
        gradient.addColorStop(1, 'rgba(0, 0, 0, 0.35)')

        this.context.save()

        // draw background
        this.context.fillStyle = this.background_colour
        this.context.fillRect(0, 0, this.canvas.width, this.canvas.height)
        // draw gradient
        this.context.fillStyle = gradient
        this.context.fillRect(0, 0, this.canvas.width, this.canvas.height)

        this.context.restore()
    }
}