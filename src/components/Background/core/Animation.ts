import CanvasObject from './CanvasObject'

/**
 *  Animation
 */
export default class Animation
{
    /**
     * @var CanvasRenderingContext2D
     */
    protected readonly context: CanvasRenderingContext2D


    /**
     * @var HTMLCanvasElement
     */
    protected readonly canvas: HTMLCanvasElement


    /**
     * CanvasObjects on canvas
     * @var CanvasObject[]  array of CanvasObjects
     */
    protected objects: Array<CanvasObject> = []


    
    /**
     * @param canvas canvas to draw on
     */
    public constructor(canvas: HTMLCanvasElement)
    {
        this.canvas = canvas
        this.context = <CanvasRenderingContext2D>this.canvas.getContext('2d')
        
        // perform resize once
        this.onResize()

        // init loop
        this.loop()
    }


    /**
     * main loop
     */
    protected loop()
    {
        this.clear()
        this.update()
        this.draw()
        this.next()
    }


    /**
     * clear the canvas
     */
    protected clear()
    {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }

    
    /**
     * request next frame
     */
    protected next()
    {
        window.requestAnimationFrame(() => this.loop())
    }



    /**
     * resize canvas to window
     */
    public onResize = () =>
    {
        this.canvas.width  = window.innerWidth
        this.canvas.height = window.innerHeight
    }



    public update()
    {
        this.updateObjects()
    }


    /**
     * run update function on all objeccts
     */
    public updateObjects()
    {
        this.objects.forEach(object =>
        {
            // if object off screen, remove it
            this.objects = this.objects.filter(
                object => object.withinBounds(0, this.canvas.width+1, 0, this.canvas.height)
            )
            object.update()    
        })
    }



    /**
     * draws the canvas
     */
    public draw()
    {
        this.drawObjects()
    }


    /**
     * Draws all objects
     */
    protected drawObjects()
    {
        this.context.save()
        this.objects.forEach(object =>
        {
            object.draw()
        })
        this.context.restore()
    }
}
