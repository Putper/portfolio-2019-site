import Vector2 from './Vector2'


export default class CanvasObject
{
    /**
     * context to draw on
     */
    public context: CanvasRenderingContext2D


    /**
     * X,Y position on canvas
     */
    protected position: Vector2


    public constructor(context:CanvasRenderingContext2D, position: Vector2)
    {
        this.context = context
        this.position = position
    }


    public draw(){}

    public update(){}


    /**
     * calculate distance between this object and another
     * @return Vector2
     */
    distanceTo(object:CanvasObject)
    {
        return this.position.distanceTo(object.position)
    }


    withinBounds(x1:number, x2:number, y1:number, y2:number)
    {
        return (
            this.position.x >= x1
            && this.position.x <= x2
            && this.position.y >= y1
            && this.position.y <= y2
        )
    }
}
