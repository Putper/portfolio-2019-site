import Vector2 from './Vector2'
import CanvasObject from './CanvasObject'


export default class PhysicalObject extends CanvasObject
{
    /**
     * Current velocity
     */
    velocity: Vector2

    /**
     * Mass of the object
     * @var mass in KG
     */
    mass: number

    /**
     * Radius
     * @var radius 1px=1cm
     */
    radius: number

    /**
     * coefficient of restitution (0-1)
     */
    restitution: number


    public constructor(context:CanvasRenderingContext2D, position:Vector2, mass:number, radius:number, restitution:number, velocity:Vector2)
    {
        super(context, position)

        this.mass = mass
        this.radius = radius
        this.restitution = restitution 
        this.velocity = velocity
    }


    public update()
    {
        super.update()
    }
}
