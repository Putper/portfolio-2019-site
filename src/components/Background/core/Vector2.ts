export default class Vector2
{
    /**
     * x pos
     */
    public x: number = 0

    /**
     * y pos
     */
    public y: number = 0


    public constructor(x:number=0, y:number=0)
    {
        this.x = x
        this.y = y
    }

    
    /**
     * Add to vector2 x&y
     * @param vector
     */
    add(vector:Vector2)
    {
        this.x += vector.x
        this.y += vector.y
    }


    /**
     * Randomise values
     * @param min
     * @param max
     * @param value string|null, null=both, x=x, y=y
     * @return Vector2
     */
    randomise(min:number=0, max:number=1, value:any=null): Vector2
    {
        // random number
        const random = Math.floor(Math.random() * (max - min + 1) + min)

        if(value!=='y')
            this.x = random
        else
            this.y = random
        
        if(value==null)
            return this.randomise(min,max,'y')

        return this
    }


    /**
     * calculate distance between this position and other
     * @param position position to calc the distance to
     * @return Vector2
     */
    distanceTo(position:Vector2)
    {
        const x = this.x - position.x
        const y = this.y - position.y
        return new Vector2( Math.abs(x), Math.abs(y) )
    }


    /**
     * Get length of vector
     */
    magnitude()
    {
        return Math.sqrt(this.x*this.x + this.y*this.y)
    }
}
