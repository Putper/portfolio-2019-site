import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/vuetify'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
import { createProvider } from './plugins/apollo'

Vue.config.productionTip = false

new Vue(
{
    router,
    apolloProvider: createProvider(),
    render: h => h(App)
}).$mount('#app')
